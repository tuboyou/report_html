import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import home from '@/components/home'
import details from '@/components/details'
import photo from '@/components/photo'
import other from '@/components/other'



Vue.use(Router)

export default new Router({

  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/details',
      name: 'details',
      component: details
    },
    {
      path: '/photo',
      name: 'photo',
      component: photo
    },
    {
      path: '/other',
      name: 'other',
      component: other
    }
  ]
})
